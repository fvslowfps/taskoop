package com.company;
import com.company.fasade.HTBuilder;
import com.company.fasade.HashList;
import com.company.fasade.HashTable;
import com.company.fasade.MyList;
import com.company.fasade.HashManager;
import com.company.pattern.GeomBank;
import com.company.pattern.GeomFigure;
import com.company.pattern.Line;
import com.company.pattern.SingleBank;

import javax.swing.text.Position;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        ArrayList<Line> lines = new ArrayList<>();
        lines.add(new Line(new Point(5, 6), new Point(7,8)));
        lines.add(new Line(new Point(6, 7), new Point(7,8)));
        lines.add(new Line(new Point(6, 7), new Point(8,9)));
        lines.add(new Line(new Point(8, 9), new Point(10,5)));
        SingleBank.getSingleBank().Add(new GeomFigure(lines, "quadrilateral"));
        System.out.println(SingleBank.getSingleBank().Copy(0).toString());
    }
}

