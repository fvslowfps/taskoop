package com.company.pattern;


import java.util.ArrayList;

public class GeomFigure implements Prototype {
    ArrayList <Line> lines;
    String title;

    public GeomFigure(ArrayList<Line> a, String b )
    {
        lines = a;
        title = b;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<Line> getLines() {
        return lines;
    }

    @Override
    public Prototype clone() {
        GeomFigure clone = new GeomFigure(lines, title);
        return clone;
    }

    @Override
    public String toString() {
        return "GeomFigure{" +
                "lines=" + lines +
                ", title='" + title + '\'' +
                '}';
    }
}
