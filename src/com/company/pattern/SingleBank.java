package com.company.pattern;

public class SingleBank extends GeomBank{
    private static SingleBank singleBank;
    public static synchronized SingleBank getSingleBank(){
        if(singleBank == null){
            singleBank = new SingleBank();
        }
        return singleBank;
    }
    private SingleBank(){
        
    }
}
