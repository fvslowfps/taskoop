package com.company.pattern;

import java.util.ArrayList;

public class GeomBank {
    ArrayList<GeomFigure> bank = new ArrayList<>();
    public void Add(GeomFigure a){
        bank.add(a);
    }
    public GeomFigure Copy(int i) throws Exception {
        if(i < bank.size() && i>=0)
        {
            GeomFigure a = new GeomFigure(bank.get(i).getLines(), bank.get(i).title);
            return a;
        }
        else {
            throw new Exception("Wrong index of GeomBank");
        }
    }

}
