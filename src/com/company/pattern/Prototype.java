package com.company.pattern;

public interface Prototype {
    public Prototype clone();
}
