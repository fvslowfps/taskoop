package com.company.pattern;
import java.awt.*;

public class Line {
    public Point a;
    public Point b;

    public Line(Point a, Point b) {
        this.a = a;
        this.b = b;
    }
}
