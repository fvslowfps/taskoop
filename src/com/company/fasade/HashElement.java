package com.company.fasade;

public class HashElement<T> implements DataUnit{
    public int key;

    public T value;
    protected HashElement previous;

    public HashElement(T a, HashElement b) {
        value = a;
        previous = b;
        key = Hash(a);
    }
    private int Hash(T a){
        return a.hashCode()/10000000;
    }

    public HashElement getPrevious() {
        return previous;
    }
}
