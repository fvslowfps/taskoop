package com.company.fasade;

public class HashList<T> implements DataSet{
    HashElement<T> head;
    int length = 1;
    public HashList(Object a){
        head = new HashElement(a, null);
    }
    public void add(Object a){
        head = new HashElement(a, head);
        length ++;
    }
    public void remove(){
        int templen = length - 1;
        if(templen > 0) {
            HashElement temp = head.getPrevious();
            head = temp;
            temp = null;
            length--;
        }
    }
    public HashElement get(int n){
        if(length >= n-2) {
            HashElement curr = head.getPrevious();
            for (int i = 0; i < n - 1; i++) {
                curr = curr.getPrevious();
            }
            return curr;
        }
        else throw new RuntimeException("Sorry, this element is not existing!");
    }
}
