package com.company.fasade;

public class HashTable<T> {
    HashList<T> list;
    String[] values = new String[2000];
    public HashTable(HashList<T> a){
        if(a.length<200) {
            list = a;
            for (int i = 0; i < a.length; i++) {
                int key = a.get(i).key;
                String value = (String) a.get(i).value;
                if(key<0){
                    values[values.length/2+key] = value;
                }
                else{
                    values[key] = value;
                }
            }
        }
        else throw new RuntimeException("Your HashList's length is bigger than 200");
    }
    public void add(HashElement a){
        int key = a.key;
        String value = (String) a.value;
        if(key<0){
            values[values.length/2+key] = value;
        }
        else{
            values[key] = value;
        }
    }
    public String get(int key){
        if(key<0){
            return values[values.length/2+key];
        }
        else return values[key];
    }
}
