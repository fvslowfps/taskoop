package com.company.fasade;
//This class provide to build HashTable from different List types
public class HTBuilder <T> {
    public HashTable<T> result;
    public HashList<T> processed;
    public HTBuilder(){
        System.out.println("HashTable Builder initialized");
    }
    public HashTable createTable(HashList<T> a){
        System.out.println("Casting your HashList to HashTable");
        result = new HashTable<T>(a);
        return result;
    }
    public HashTable createTable(MyList<T> a){
        System.out.println("Recasting your List to Hashlist, HashList to HashTable");
        processed = new HashList<>(a.head.value);
        for(int i = 0; i<a.length; i++){
            processed.add(a.get(i));
        }
        return new HashTable(processed);
    }
}
