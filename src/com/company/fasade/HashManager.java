package com.company.fasade;

import java.util.ArrayList;

//This class provide to collect commands for HashTable without using it
public class HashManager <T>{
    ArrayList<T> values = new ArrayList<>();
    ArrayList<Integer> requests = new ArrayList<Integer>();
    HashTable inWorking;
    public HashManager(HashTable a){
        inWorking = a;
    }
    public void AddValue(T a){
        values.add(a);
    }
    public void AddReq(int a){
        requests.add(a);
    }
    public void Process(){
        System.out.println("Processing HashTable initialized");
        for(T a: values){
            inWorking.add(new HashElement(a, null));
        }
        for(int a: requests){
            System.out.println(inWorking.get(a));
        }
    }
}
