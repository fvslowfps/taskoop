package com.company.fasade;

public interface DataSet <T>
{
    public void add(T a);
    public void remove();
    public T get(int n);
}
