package com.company.fasade;

public class Element<T> implements DataUnit{
    public T value;
    protected Element previous;

    public Element(T a, Element b) {
        value = a;
        previous = b;
    }

    public Element getPrevious() {
        return previous;
    }
}
