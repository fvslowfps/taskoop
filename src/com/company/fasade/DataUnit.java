package com.company.fasade;

public interface DataUnit{
    public DataUnit getPrevious();
}
