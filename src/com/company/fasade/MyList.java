package com.company.fasade;

public class MyList<T> implements DataSet {

    Element<T> head;
    int length = 1;
    public MyList(Object a){
        head = new Element(a, null);
    }
    public void add(Object a){
        head = new Element (a, head);
        length ++;
    }
    public void remove(){
        int templen = length - 1;
        if(templen > 0) {
            Element temp = head.getPrevious();
            head = temp;
            temp = null;
            length--;
        }
    }
    public T get(int n){
        if(length >= n-2) {
            Element<T> curr = head.getPrevious();
            for (int i = 0; i < n - 1; i++) {
                curr = curr.getPrevious();
            }
            return curr.value;
        }
        else throw new RuntimeException("Sorry, this element is not existing!");
    }

}
